import sys
import random
import pygame

from pygame.locals import *
from locals import *


def getRandomizedBoard():
    icons = []

    for shape in ALL_SHAPES:
        for color in ALL_COLORS:
            icons.append((shape, color))

    random.shuffle(icons)

    numIconsUsed = BOARD_WIDTH * BOARD_HEIGHT // 2
    icons = icons[:numIconsUsed] * 2

    random.shuffle(icons)

    board = []
    icon_index = 0

    for x in range(BOARD_WIDTH):
        column = []

        for y in range(BOARD_HEIGHT):
            column.append(icons[icon_index])
            icon_index += 1

        board.append(column)

    return board


def generateRevealedBoxesData(val):
    revealedBoxes = []

    for i in range(BOARD_WIDTH):
        revealedBoxes.append([val] * BOARD_HEIGHT)

    return revealedBoxes


def drawBoard(board, revealed):
    for box_x in range(BOARD_WIDTH):
        for box_y in range(BOARD_HEIGHT):
            box_pos = (box_x, box_y)

            left, top = leftTopCoordsOfBox(box_pos)

            if not revealed[box_x][box_y]:
                pygame.draw.rect(DISPLAY, BOX_COLOR, (left, top, BOX_SIZE, BOX_SIZE))
            else:
                shape, color = getShapeAndColor(board, box_pos)
                drawIcon(shape, color, box_pos)


def drawHighlightBox(box_pos):
    left, top = leftTopCoordsOfBox(box_pos)
    half = GAP_SIZE // 2

    pygame.draw.rect(DISPLAY, HIGHLIGHT_COLOR, (left - half, top - half, BOX_SIZE + GAP_SIZE, BOX_SIZE + GAP_SIZE), half - 1)


def leftTopCoordsOfBox(pos):
    left = pos[0] * (BOX_SIZE + GAP_SIZE) + X_MARGIN
    top  = pos[1] * (BOX_SIZE + GAP_SIZE) + Y_MARGIN

    return (left, top)


def getBoxAtPixel(pos):
    for box_x in range(BOARD_WIDTH):
        for box_y in range(BOARD_HEIGHT):
            box_pos = (box_x, box_y)

            left, top = leftTopCoordsOfBox(box_pos)
            rect = pygame.Rect(left, top, BOX_SIZE, BOX_SIZE)

            if rect.collidepoint(pos[0], pos[1]):
                return box_pos


def revealBoxesAnimation(board, boxesToReveal):
    for coverage in range(BOX_SIZE, (-REVEAL_SPEED) - 1, - REVEAL_SPEED):
        drawBoxCovers(board, boxesToReveal, coverage)


def coverBoxesAnimation(board, boxesToCover):
    for coverage in range(0, BOX_SIZE + REVEAL_SPEED, REVEAL_SPEED):
        drawBoxCovers(board, boxesToCover, coverage)


def hasWon(revealed):
    for x in range(BOARD_WIDTH):
        for y in range(BOARD_HEIGHT):
            if not revealed[x][y]:
                return False
    return True


def getCoveredAmount(revealed):
    c = 0

    for x in range(BOARD_WIDTH):
        for y in range(BOARD_HEIGHT):
            c += not revealed[x][y]

    return c

def gameWonAnimation(board):
    coveredBoxes = generateRevealedBoxesData(True)

    color1 = LIGHT_BG_COLOR
    color2 = BG_COLOR

    for i in range(13):
        color1, color2 = color2, color1

        DISPLAY.fill(color1)

        drawBoard(board, coveredBoxes)

        pygame.display.update()
        pygame.time.wait(300)


def drawIcon(shape, color, box_pos):
    quarter = BOX_SIZE // 4
    half = BOX_SIZE // 2

    left, top = leftTopCoordsOfBox(box_pos)

    if shape == DONUT:
        pygame.draw.circle(DISPLAY, color, (left + half, top + half), half - 5)
        pygame.draw.circle(DISPLAY, BG_COLOR, (left + half, top + half), quarter - 5)
    elif shape == SQUARE:
        pygame.draw.rect(DISPLAY, color, (left + quarter, top + quarter, half, half))
    elif shape == DIAMOND:
        pygame.draw.polygon(DISPLAY, color, ((left + half, top), (left + BOX_SIZE - 1, top + half), (left + half, top + BOX_SIZE - 1), (left, top + half)))
    elif shape == LINES:
        for i in range(0, BOX_SIZE, 4):
            pygame.draw.line(DISPLAY, color, (left, top + i), (left + i, top))
            pygame.draw.line(DISPLAY, color, (left + i, top + BOX_SIZE - 1), (left + BOX_SIZE - 1, top + i))
    elif shape == OVAL:
        pygame.draw.ellipse(DISPLAY, color, (left, top + quarter, BOX_SIZE, half))


def drawBoxCovers(board, boxes, coverage):
    for box_pos in boxes:
        left, top = leftTopCoordsOfBox(box_pos)
        pygame.draw.rect(DISPLAY, BG_COLOR, (left, top, BOX_SIZE, BOX_SIZE))

        shape, color = getShapeAndColor(board, box_pos)
        drawIcon(shape, color, box_pos)

        if coverage > 0:
            pygame.draw.rect(DISPLAY, BOX_COLOR, (left, top, coverage, BOX_SIZE))

    pygame.display.update()

    FPS_CLOCK.tick(FPS)


def getShapeAndColor(board, box_pos):
    return board[box_pos[0]][box_pos[1]][0], board[box_pos[0]][box_pos[1]][1]


def splitIntoGroups(theList, groupSize):
    result = []

    for i in range(0, len(theList), groupSize):
        result.append(theList[i:i + groupSize])

    return result


def startGameAnimation(board):
    DISPLAY.fill(BG_COLOR)

    coveredBoxes = generateRevealedBoxesData(False)
    boxes = []

    for x in range(BOARD_WIDTH):
        for y in range(BOARD_HEIGHT):
            boxes.append((x, y))

    random.shuffle(boxes)
    boxGroups = splitIntoGroups(boxes, GROUP_SIZE)

    drawBoard(board, coveredBoxes)

    for boxGroup in boxGroups:
        revealBoxesAnimation(board, boxGroup)
        coverBoxesAnimation(board, boxGroup)


def startGame(board, revealedBoxes):
    startGameAnimation(board)

    firstSelection = None
    secondSelection = None

    mouse_pos = (0, 0)

    while True:
        DISPLAY.fill(BG_COLOR)

        drawBoard(board, revealedBoxes)

        mouseClicked = False

        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()
            elif event.type == MOUSEMOTION:
                mouse_pos = event.pos
            elif event.type == MOUSEBUTTONUP:
                mouse_pos = event.pos
                mouseClicked = True

        box_pos = getBoxAtPixel(mouse_pos)

        if box_pos != None:
            if not revealedBoxes[box_pos[0]][box_pos[1]]:
                drawHighlightBox(box_pos)

            if not revealedBoxes[box_pos[0]][box_pos[1]] and mouseClicked:
                revealBoxesAnimation(board, [box_pos])
                revealedBoxes[box_pos[0]][box_pos[1]] = True

                coveredAmount = getCoveredAmount(revealedBoxes)

                if firstSelection == None and coveredAmount > 0:
                    firstSelection = box_pos
                elif secondSelection == None and coveredAmount > 0:
                    secondSelection = box_pos
                else:
                    icon1 = getShapeAndColor(board, firstSelection) if coveredAmount > 0 else None
                    icon2 = getShapeAndColor(board, secondSelection) if coveredAmount > 0 else None
                    icon3 = getShapeAndColor(board, box_pos)

                    if icon1 == None and icon2 == None:
                        pass

                    elif icon1 == icon2 != icon3:
                        pygame.time.wait(1000)

                        coverBoxesAnimation(board, [box_pos])
                        revealedBoxes[box_pos[0]][box_pos[1]] = False

                    elif icon1 == icon3 != icon2:
                        pygame.time.wait(1000)

                        if secondSelection != None:
                            coverBoxesAnimation(board, [secondSelection])
                            revealedBoxes[secondSelection[0]][secondSelection[1]] = False

                    elif icon2 == icon3 != icon1:
                        pygame.time.wait(1000)

                        coverBoxesAnimation(board, [firstSelection])
                        revealedBoxes[firstSelection[0]][firstSelection[1]] = False

                    elif icon1 != icon2 != icon3:
                        pygame.time.wait(1000)

                        coverBoxesAnimation(board, [firstSelection, secondSelection, box_pos])

                        revealedBoxes[firstSelection[0]][firstSelection[1]] = False
                        revealedBoxes[secondSelection[0]][secondSelection[1]] = False
                        revealedBoxes[box_pos[0]][box_pos[1]] = False

                    if hasWon(revealedBoxes):
                        gameWonAnimation(board)

                        pygame.time.wait(2000)

                        board = getRandomizedBoard()
                        revealedBoxes = generateRevealedBoxesData(False)

                        drawBoard(board, revealedBoxes)

                        pygame.display.update()

                        pygame.time.wait(1000)
                        startGame(board, revealedBoxes)

                    firstSelection = None
                    secondSelection = None

        pygame.display.update()

        FPS_CLOCK.tick(FPS)


def main():
    global DISPLAY, FPS_CLOCK

    pygame.init()

    FPS_CLOCK = pygame.time.Clock()
    DISPLAY = pygame.display.set_mode(DISPLAY_SIZE)

    pygame.display.set_caption(DISPLAY_CAPTION)

    board = getRandomizedBoard()
    revealedBoxes = generateRevealedBoxesData(False)

    startGame(board, revealedBoxes)


if __name__ == '__main__':
    main()
